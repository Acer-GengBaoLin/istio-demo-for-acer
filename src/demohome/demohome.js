var axios = require('axios');
var express = require('express');
var app = express();

// Our first route
app.get('/', function (req, res) {
    var content = 'This is a Istio demo app by GengBao.Lin@acer.com <br /> \n';
    axios.get('http://democontent:8080/').then((resa) => {
        content += resa.data + '<br /> \n';
    	axios.get('http://democontent:8080/secondMsg').then((resb) => {
    	    content += resb.data + '<br /> \n';
    	    res.send(content);
    	}).catch(() => {
            content += '<b>Fetch data error!</b><br /> \n';
            res.send(content);
        });
    });
});

// Listen to port 8080
app.listen(8080, function () {
    console.log('App listening!');
});
