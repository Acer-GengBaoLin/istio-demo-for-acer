var express = require('express');
var app = express();

// Our first route
app.get('/', function (req, res) {
    res.send('Nice, This line from <b>v4</b> content service with route /');
});

// Our broke route
app.get('/broke', function (req, res) {
    res.send('Nice, This line from <b>v4</b> content service with route /broke');
});

// Listen to port 8080
app.listen(8080, function () {
    console.log('App listening!');
});
